# Tildes Blog and Docs

This repo contains the source files used to generate the [Tildes Blog](https://blog.tildes.net) and [Tildes Docs](https://docs.tildes.net) static sites, by using [Pelican](http://docs.getpelican.com/).

## Contributions

**Important:** If you just want to contribute some changes to the content of the [Tildes Docs](https://docs.tildes.net), there is a simpler way than interacting with this repo: the Docs pages are duplicated in [the wiki for the ~tildes.official group on Tildes itself](https://tildes.net/~tildes.official/wiki). Edits made there will be reviewed and moved to the Docs site. To request access to edit the wiki pages, [send Deimos a message on Tildes](https://tildes.net/user/Deimos/new_message).

---

In general, contributions (edits or adding new pages) are welcome for the Docs site (files inside the `content/pages/` directory). However, please do not submit edits to pages inside the Policies subdirectory without prior discussion/approval.

The Blog posts (files in the base `content/` directory) are not open for user contributions, and are mostly included here to make their edit history publicly available.

You may also contribute changes to the theme and templates, but if you do so, keep in mind that the theme is shared across both the Blog and Docs sites and changes may affect both.

## Setup and Running

Before you submit a merge request, you should check that your markdown doc rendered correctly by running Pelican on your own machine. You can install Pelican along with the other required libraries with:

```bash
pip install pelican markdown beautifulsoup4 html5lib
```

Then, to generate the static files, you can run:

```bash
pelican content
```

This will create an `output` folder that contains the generated HTML files. If you'd like to run it as a webserver, `cd` into the `output` folder and then run:

```bash
python -m pelican.server
```

Then you can connect to your local version of the site on [http://localhost:8000](http://localhost:8000).

## License

* Docs pages are licensed under [Creative Commons Attribution-ShareAlike 4.0 (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/).
* Blog posts are not licensed, and are the property of their respective authors.
* Source code used to format the pages (theme, templates, configuration, plugins) is licensed under [the MIT license](https://opensource.org/licenses/MIT).

Any contributions will be licensed under the same terms.
